# StopWatch by Benjapol Worakan (5710546577)

This StopWatch project will run the predefined tasks using the local JVM on your computer. And display the time your computer spent running those tasks.

* * *

# Test run

I ran these tasks on my Dell Vostro 5460 with an Intel Core i3-3120M processor, running Windows 10 Technical Preview (Build 9926) and Java SE 1.8, and got these results :

    +-[TASK]---------------------------------------------------+-[TIME]-------------+
    |                Appending char to String for 100000 times |          5.680512s |
    +----------------------------------------------------------+--------------------+
    +-[TASK]---------------------------------------------------+-[TIME]-------------+
    |         Appending char to StringBuilder for 100000 times |          0.004234s |
    +----------------------------------------------------------+--------------------+
    +-[TASK]---------------------------------------------------+-[TIME]-------------+
    |   Summing double[500000] (primitive) for 100000000 times |          0.393501s |
    +----------------------------------------------------------+--------------------+
    +-[TASK]---------------------------------------------------+-[TIME]-------------+
    |      Summing Double[500000] (object) for 100000000 times |          1.000087s |
    +----------------------------------------------------------+--------------------+
    +-[TASK]---------------------------------------------------+-[TIME]-------------+
    |           Summing BigDecimal[500000] for 100000000 times |          1.677395s |
    +----------------------------------------------------------+--------------------+

* * *

## Explanation of Results

The results show that appending chars to a String seems to be the most difficult task for the computer, and it's much more slower than appending chars to StringBuilder (like 1000 times slower). I think it's because Strings are immutable, so Java needs to create a new String object every time we append a char to String (or modify that String in anyway). But for StringBuilder, it contains a mutable String buffer, so Java can just append that single char into a StringBuilder object.

And summing BigDecimals is also a hard work for the computer, because they are immutable too. A new BigDecimal object has to be created every time that we made any changes to an existing BigDecimal object.

Summing Double objects take longer than summing double primitives just because a Double object is actually a wrapper of a double primitive. Wrappers of primitives are not mutable (immutable). So when summing Doubles, Java has create a new Double object. But when summing doubles, it doesn't have to. 