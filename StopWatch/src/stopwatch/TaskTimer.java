package stopwatch;

/**
 * Task runner that run the received task and display the information and time
 * spent on running that task.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 2015.01.30
 */
public class TaskTimer {
	/**
	 * Run the task, measure the runtime and display information about the task
	 * and time spent running the task.
	 * 
	 * @param task
	 *            is the task to be run and measure the run time.
	 */
	public static void measureAndPrint(Runnable task) {
		/** Create a StopWatch that will measure the run time of the task. */
		StopWatch timer = new StopWatch();
		
		/** Start the StopWatch. */
		timer.start();
		
		/** Run the task. */
		task.run();
		
		/** Stop the StopWatch */
		timer.stop();
		
		System.out.println("+-[TASK]-------------------------------------------"
				+ "--------+-[TIME]-------------+");
		System.out.printf("| %56s | %17.6fs |\n",
				task.toString(), timer.getElapsed());
		System.out.println("+--------------------------------------------------"
				+ "--------+--------------------+");
	}
}
