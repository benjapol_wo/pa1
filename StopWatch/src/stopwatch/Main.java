package stopwatch;

import java.math.BigDecimal;

/**
 * Entry point of the program. Will create and run predefined tasks and display
 * time used in each task.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 2015.01.30
 */
public class Main {
	/**
	 * Entry point of the application. Will create tasks and pass them over to
	 * TaskTimer to run and display results.
	 * 
	 * @param args
	 *            not used
	 */
	public static void main(String[] args) {
		/** For-each loop create tasks that passes each task to TaskTimer */
		for (Runnable r : new Runnable[] {
				new AppendToStringTask(100000),
				new AppendToStringBuilderTask(100000),
				new SumDoublePrimitiveTask(100000000, 500000),
				new SumDoubleTask(100000000, 500000),
				new SumBigDecimalTask(100000000, 500000)
		}) {
			TaskTimer.measureAndPrint(r);
		}
	}
}

/**
 * A task that append a char to String for defined times.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 2015.01.30
 */
class AppendToStringTask implements Runnable {
	/** Amount of times that this task will recurs. */
	private long limit;

	/**
	 * Create and initialize this task.
	 * 
	 * @param limit
	 *            is times that this task will recurs.
	 */
	public AppendToStringTask(long limit) {
		this.limit = limit;
	}

	/**
	 * Re-set the limit of this task.
	 * 
	 * @param limit
	 *            is times that this task will recurs.
	 */
	public void setLimit(long limit) {
		this.limit = limit;
	}

	/**
	 * Execute this task.
	 */
	@Override
	public void run() {
		final char CHAR = 'a';
		String sum = "";
		int k = 0;
		while (k++ < limit) {
			sum = sum + CHAR;
		}
	}

	/**
	 * Get a String representation of this task.
	 * 
	 * @return a String representation of this task.
	 */
	@Override
	public String toString() {
		return "Appending char to String for " + limit + " times";
	}
}

/**
 * A task that append a char to StringBuilder for defined times.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 2015.01.30
 */
class AppendToStringBuilderTask implements Runnable {
	/** Amount of times that this task will recurs. */
	private long limit;

	/**
	 * Create and initialize this task.
	 * 
	 * @param limit
	 *            is times that this task will recurs.
	 */
	public AppendToStringBuilderTask(long limit) {
		this.limit = limit;
	}

	/**
	 * Re-set the limit of this task.
	 * 
	 * @param limit
	 *            is times that this task will recurs.
	 */
	public void setLimit(long limit) {
		this.limit = limit;
	}

	/**
	 * Execute this task.
	 */
	@Override
	public void run() {
		final char CHAR = 'a';
		StringBuilder builder = new StringBuilder();
		int k = 0;
		while (k++ < limit) {
			builder = builder.append(CHAR);
		}
	}

	/**
	 * Get a String representation of this task.
	 * 
	 * @return a String representation of this task.
	 */
	@Override
	public String toString() {
		return "Appending char to StringBuilder for " + limit + " times";
	}
}

/**
 * A task that add double primitives from an array. Needs a large limit to get a
 * measurable time.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 2015.01.30
 */
class SumDoublePrimitiveTask implements Runnable {
	/** Amount of times that this task will recurs. */
	private long limit;
	/** Array of double primitives that will be used when executing this task. */
	private double[] values;

	/**
	 * Create and initialize this task.
	 * 
	 * @param limit
	 *            is times that this task will recurs.
	 * @param arraySize
	 *            is the size of the array that will be created for this task.
	 */
	public SumDoublePrimitiveTask(long limit, int arraySize) {
		this.limit = limit;
		values = new double[arraySize];
		for (int k = 0; k < arraySize; k++) {
			values[k] = k + 1;
		}
	}

	/**
	 * Re-set the limit of this task.
	 * 
	 * @param limit
	 *            is times that this task will recurs.
	 */
	public void setLimit(long limit) {
		this.limit = limit;
	}

	/**
	 * Execute this task.
	 */
	@Override
	public void run() {
		double sum = 0.0;
		for (int count = 0, i = 0; count < limit; count++, i++) {
			if (i >= values.length) {
				i = 0;
			}
			sum = sum + values[i];
		}
	}

	/**
	 * Get a String representation of this task.
	 * 
	 * @return a String representation of this task.
	 */
	@Override
	public String toString() {
		return "Summing double[" + values.length + "] (primitive) for " + limit
				+ " times";
	}

}

/**
 * A task that add Double objects from an array. Needs a large limit to get a
 * measurable time.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 2015.01.30
 */
class SumDoubleTask implements Runnable {
	/** Amount of times that this task will recurs. */
	private long limit;
	/** Array of Double objects that will be used when executing this task. */
	private Double[] values;

	/**
	 * Create and initialize this task.
	 * 
	 * @param limit
	 *            is times that this task will recurs.
	 * @param arraySize
	 *            is the size of the array that will be created for this task.
	 */
	public SumDoubleTask(long limit, int arraySize) {
		this.limit = limit;
		values = new Double[arraySize];
		for (int i = 0; i < arraySize; i++) {
			values[i] = new Double(i + 1);
		}

	}

	/**
	 * Re-set the limit of this task.
	 * 
	 * @param limit
	 *            is times that this task will recurs.
	 */
	public void setLimit(long limit) {
		this.limit = limit;
	}

	/**
	 * Execute this task.
	 */
	@Override
	public void run() {
		Double sum = new Double(0.0);
		for (int count = 0, i = 0; count < limit; count++, i++) {
			if (i >= values.length) {
				i = 0;
			}
			sum = sum + values[i];
		}
	}

	/**
	 * Get a String representation of this task.
	 * 
	 * @return a String representation of this task.
	 */
	@Override
	public String toString() {
		return "Summing Double[" + values.length + "] (object) for " + limit
				+ " times";
	}
}

/**
 * A task that add BigDecimal objects from an array. Needs a large limit to get
 * a measurable time.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 2015.01.30
 *
 */
class SumBigDecimalTask implements Runnable {
	/** Amount of times that this task will recurs. */
	private long limit;
	/** Array of BigDecimal objects that will be used when executing this task. */
	private BigDecimal[] values;

	/**
	 * Create and initialize this task.
	 * 
	 * @param limit
	 *            is times that this task will recurs.
	 * @param arraySize
	 *            is the size of the array that will be created for this task.
	 */
	public SumBigDecimalTask(long limit, int arraySize) {
		this.limit = limit;
		values = new BigDecimal[arraySize];
		for (int i = 0; i < arraySize; i++) {
			values[i] = new BigDecimal(i + 1);
		}
	}

	/**
	 * Re-set the limit of this task.
	 * 
	 * @param limit
	 *            is times that this task will recurs.
	 */
	public void setLimit(long limit) {
		this.limit = limit;
	}

	/**
	 * Execute this task.
	 */
	@Override
	public void run() {
		BigDecimal sum = new BigDecimal(0.0);
		for (int count = 0, i = 0; count < limit; count++, i++) {
			if (i >= values.length) {
				i = 0;
			}
			sum = sum.add(values[i]);
		}
	}

	/**
	 * Get a String representation of this task.
	 * 
	 * @return a String representation of this task.
	 */
	@Override
	public String toString() {
		return "Summing BigDecimal[" + values.length + "] for " + limit
				+ " times";
	}

}
